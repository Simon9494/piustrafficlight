package ch.fhnw.masam.restservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class FHNWMathTest {
	
	@Test
	public void powerTest() {
		FHNWMath obj = new FHNWMath();
		
		assertEquals(1.0, obj.power(2, 0));
		assertEquals(2.0, obj.power(2, 1));
		assertEquals(8.0, obj.power(2, 3));
	}
}

package ch.fhnw.masam.restservice;

import java.util.HashMap;
import java.util.Map;

public class MockGPIOUtil implements IGPIOUtil {

    private Map<Integer, Integer> pins = new HashMap<>();

    public void setState(int pin, int state) throws Exception {
        pins.put(pin, state);
    }

    public boolean getState(int pin) throws Exception {
        if (!pins.containsKey(pin)) {
            pins.put(pin, STATE_LOW);
        }
        boolean result = pins.get(pin) == STATE_HIGH ? true : false;
        return result;
    }
}

package ch.fhnw.masam.restservice;

public interface IGPIOUtil {

    int STATE_HIGH = 1;
    int STATE_LOW = 0;

    void setState(int pin, int state) throws Exception;

    boolean getState(int pin) throws Exception;

}
